﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace yors.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Youth Organization Reward Systems";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "YORS Contacts";

            return View();
        }
    }
}