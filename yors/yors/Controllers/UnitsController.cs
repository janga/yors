﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using yors.Models;
using yors.ViewModels;

namespace yors.Controllers
{
    public class UnitsController : Controller
    {
        private static readonly UnitModels Context = new UnitModels();
        private static readonly ClaimsIdentity Identity = new ClaimsIdentity();

        // GET: Units
        public ActionResult ApplyUnits()
        {
            return View("ApplyUnits", LoadMenuViewModel());
        }

        [HttpPost]
        public ActionResult ApplyUnits(FormCollection form)
        {         
            if(form["Units"] != null && form["Units"] != string.Empty)
                Context.SaveUnitRequest(User.Identity.GetUserId(), form["Units"]);

            return View("ApplyUnits", LoadMenuViewModel());
        }

        private MenuViewModel LoadMenuViewModel()
        {
            var menuViewModel = new MenuViewModel
            {
                Units = Context.GetUnitList(User.Identity.GetUserId()),
                RequestList = Context.GetUnitRequestList(User.Identity.GetUserId()),
                ConfirmedList = Context.GetUnitConfirmedList(User.Identity.GetUserId())
            };

            return menuViewModel;
        }

        [ChildActionOnly]
        public ActionResult UnitMenu()
        {
            return PartialView(LoadMenuViewModel().ConfirmedList.AsEnumerable());
        }

    }
}