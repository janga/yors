﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yors.EDMX;

namespace yors.Models
{
    public class UnitModels
    {
        public DB_110913_yorsEntities2 DbContext;

        public UnitModels()
        {
            DbContext = new DB_110913_yorsEntities2();
        }

        public List<SelectListItem> GetUnitList(string userId)
        {

            var searchUr = DbContext.User_Unit_Request.Where(ur => ur.UserId == userId).Select(un => new SelectListItem { Value = un.UnitId.ToString(), Text = un.UnitId.ToString() });
            var searchUu = DbContext.User_Unit.Where(uu => uu.UserId == userId).Select(un => new SelectListItem { Value = un.UnitId.ToString(), Text = un.UnitId.ToString() });

            return DbContext.Units.Where(u => !searchUr
                                                  .Any(ur => ur.Text == u.UnitId.ToString()) && !searchUu.Any(uu => uu.Text == u.UnitId.ToString()))
            .Select(un => new SelectListItem { Value = un.UnitId.ToString(), Text = un.UnitName }).ToList();
        }

        public List<SelectListItem> GetUnitRequestList(string userId)
        {
            var result = DbContext.User_Unit_Request.Where(ur => ur.UserId == userId);
            return DbContext.Units.Join(result, u => u.UnitId, r => r.UnitId, (u, r) => new SelectListItem {Value = u.UnitId.ToString(), Text = u.UnitName}).ToList();
        }

        public List<SelectListItem> GetUnitConfirmedList(string userId)
        {
            var result = DbContext.User_Unit.Where(ur => ur.UserId == userId);
            return DbContext.Units.Join(result, u => u.UnitId, r => r.UnitId, (u, r) => new SelectListItem { Value = u.UnitId.ToString(), Text = u.UnitName }).ToList();
        }

        public bool SaveUnitRequest(string userId, string requestedUnitIds)
        {
            try
            {
                var unitIds = requestedUnitIds.Split(',').ToList();
                foreach (var item in unitIds)
                {
                    DbContext.User_Unit_Request.Add(new User_Unit_Request()
                    { CreatedByUserId = userId, CreatedDate = DateTime.Now, ModifiedByUserId = userId, ModifiedDate = DateTime.Today,
                        UnitId = new Guid(item), UserId = userId,UserUnitRequestId = Guid.NewGuid()});
                }

                DbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }         
        }
    }
}