﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(yors.Startup))]
namespace yors
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
