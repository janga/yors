﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using yors.Models;
using System.Web.Mvc;

namespace yors.ViewModels
{
    public class MenuViewModel
    {
        public List<SelectListItem> Units { get; set; }
        public List<SelectListItem> RequestList { get; set; }
        public List<SelectListItem> ConfirmedList { get; set; }
        public IEnumerable<SelectListItem> ConfirmedMenuList { get; set; }
    }
}